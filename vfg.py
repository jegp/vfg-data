import urlparse, urllib2, urllib
from urlparse import urljoin
from bs4 import BeautifulSoup
from pyPdf import PdfFileWriter, PdfFileReader
from StringIO import StringIO

#A script for generating the Danish AIP/VFG in one PDF file. 
#See 2/3 down for what to comment in/out to generate VFG or AIP.

def find_links(url, search):
      source = urllib2.urlopen(url).read()
      soup   = BeautifulSoup(source)
      pdfs   = []
      for td in soup.findAll("td", {"class" : "ms-vb2"}):
            for item in td.findAll('a'):
                  try:
                        href = item['href']
                        new_url = urljoin(url, href)
                        if ".pdf" in href:
                              print "Found: " + href
                              if "AMDT" in href:                             
                                    print ("omits AMDT page: ", href)
                                    pass
                              elif "KORT" in href:                             
                                    print ("omits large VFR chart: ", href)
                                    pass
                              elif "EK_ENR_6_ANC" in href:                             
                                    print ("omits large VFR chart: ", href)
                                    pass
                              elif "EK_ENR_ANCS" in href:                             
                                    print ("omits large VFR chart: ", href)
                                    pass
                              #elif "EK_ENR_6_HMR" in href:                             
                              #      print ("omits helicopter main routes: ", href)
                              #      pass
                              else: 
				    print ("adding page")
                                    pdfs.append(new_url)
                              pass
                        if search == "" or search in item.contents[0]:
                              pdfs.extend(find_links(new_url, ""))
                  except:
                        pass
      return pdfs

#USE THIS LINK FOR THE AIP (remove # in front at the beginning of the line)
#links = find_links("http://aim.naviair.dk", "AIP Danmark")

#USE THIS LINK FOR THE VFG (remove # in front at the beginning of the line)
links = find_links("http://aim.naviair.dk", "VFR Flight Guide Danmark")

pdfs = []
id = 0
fejl = 0
writer = PdfFileWriter()

print "Links found, starting download..."
for link in links:
      try:
            print "Downloading " + link
            url = link.replace(' ', '%20')
            reader = urllib2.urlopen(url).read()
            memory = StringIO(reader)
            pdfFile = PdfFileReader(memory)
            for page in xrange(pdfFile.getNumPages()):
                  writer.addPage(pdfFile.getPage(page))
      except:
        print ("could not add this page: ", link)
        #fejl = fejl + link
	pass
#output = open("AIPDENMARK.pdf", "wb")
output = open("VFGDENMARK.pdf", "wb")
writer.write(output)
output.close()
print ("The VFG/AIP is generated. ", fejl, " pages could not be downloaded")

# VFG and AIP data collection for pilots

A given contry's AIP or VFG is a invaluable help for pilots as they contain airport diagrams and approach procedures for all mayor airports. 
Often these documents are made available online for free download, but as seperarate sheets (one sheet per PDF). Being large documents of often more than 200 pages,
it is time consuming to collect a full set for printing / viewing on a tablet.

This script downloads all available PDF files from a given website's subdomain, and collects them into one large PDF. 

## Requirements

Requires python 2.7, [Beautiful Soup](http://www.crummy.com/software/BeautifulSoup/), [PyPDF](https://pypi.python.org/pypi/pyPdf), a pretty solid internet connection and about a gigabyte of RAM.

## Usage

Using a console, navigate to the repository folder and write:
```python
python vfg.py
```
Upon execution, all relevant AIP documents from AIP Denmark are fetched in order and added to one PDF.
After a while the script will store ```vfg.pdf``` in the same folder.

Currently the script excludes AIRAC AMDT og AIP AMDT files (as newest revisions are in the AIP) 
as well as the VFR chart in 1:500.000 and 1:250.000, which are impractical to store in the PDF'en (very large files)
Helicopter main routes and EKBI glider areas are also omitted because d/l of these files are currently out of service.

## Setup guide for Windows 8 (64 bit)

A bit of preparation is needed for the script to run; 
Python as well as the two libraries beautifulsoup and pyPdf must be installed.

1) get python 2.7.6 (https://www.python.org/download/releases/2.7.6/)
remember to select the 64 bit version if needed.
(Windows X86-64 MSI Installer (2.7.6) [1] (sig))

2) install python to c:\Python27

3) open the command prompt(CMD) by hitting WINDOWS-BUTTON + X and choosing command prompt

4) write the following:
	set PYTHONPATH=%PYTHONPATH%;C:\Python27 (remember small letters and caps)

5) Extracting the two libraries beautifulsoup and PyPdf requires an program to "unzip" them, such as winrar: http://www.winrar.com

6) Download beautifulsoup from
http://www.crummy.com/software/BeautifulSoup/bs4/download/4.3/beautifulsoup4-4.3.2.tar.gz

7) put it in c:\python27 and extract with winrar: right click beautifulsoup4-4.3.2.tar.gz and select "extract files". Then browse to C:/Python27
and hit "OK"), the result should be a folder called  c:/Python27/beautifulSoup4-4.3.2. Rename the folder to "beautifulsoup" (it is easier to write)
 
8) BeautifulSoup is installed into Python:
-open CMD and go to C:\Python27\beautifulsoup (write cd .. ENTER until you are in c:, then cd Python27 and cd beautifulsoup)
-now you should be in C:\python27\beautifulsoup 
-write: setup.py install if it does not work try to write: c:\Python27\python C:\Python27\beautifulsoup\setup.py install
-alot of text appears

9)download and install pyPdf using the same approach, 
the result should be a folder named c:\Python27\pyPdf
get pyPdf here: http://pybrary.net/pyPdf/pyPdf-1.13.tar.gz

## Running the script

10)now you can generate the Danish VFG! save the script from bitbucket to c:\Python27\vfg.py 
https://bitbucket.org/jegp/vfg-data/src/39cbd9ae2f92fee71fba77952805b3913663c09f/vfg.py

11) to run the script, drag VFG.py into python.exe and release the mouse.

When the script is done VFG.pdf will be located in c:\Python27 folder.

## Downloading VFG or AIP

In the script a line of code determines whether to generate the VFG or AIP (currently the VFG is generated):

#USE THIS LINK FOR THE AIP
links = find_links("http://aim.naviair.dk", "AIP Danmark")

#USE THIS LINK FOR THE VFG
links = find_links("http://aim.naviair.dk", "VFR Flight Guide Danmark")

## Omitting pages

It is easy to add/omit files in the PDF, it is done by adding/deleting three lines of code per topic. E.g:
elif "AIP AMDT" in href:                             
      print ("udelader AIP AMDT side: ", href)
      pass

in this case all PDF files under AIP AMDT are omitted. If the three lines are deleted, the files will be included. 

/OEP 2014
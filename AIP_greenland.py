import urlparse, urllib2, urllib
from urlparse import urljoin
from bs4 import BeautifulSoup
from pyPdf import PdfFileWriter, PdfFileReader
from StringIO import StringIO
from urllib import quote, unquote
import os
import sys

#A script for generating AIP GREENLAND in one PDF file. 

def find_links(url, search):
      source = urllib2.urlopen(url).read()
      soup   = BeautifulSoup(source)
      pdfs   = []
      for td in soup.findAll("td", {"class" : "ms-vb2"}):
            for item in td.findAll('a'):
                  try:
                        href = item['href']
                        new_url = urljoin(url, href)
                        if ".pdf" in href:
                              #print "Found: " + href
                              if "AMDT" in href:                             
                                    print ("omits AMDT page: ", href)
                                    pass
                              elif "KORT" in href:                             
                                    print ("omits large VFR chart: ", href)
                                    pass
                              #elif "PART 3" in href:                             
                              #      print ("omits large VFR chart: ", href)
                              #      pass      
                              elif "EK_ENR_6_ANC" in href:                             
                                    print ("omits large VFR chart: ", href)
                                    pass
                              elif "EK_ENR_ANCS" in href:                             
                                    print ("omits large VFR chart: ", href)
                                    pass
                              #elif "EK_ENR_6_HMR" in href:                             
                              #      print ("omits helicopter main routes: ", href)
                              #      pass
                              else: 
                                    print ("adding page to download-queue")
                                    pdfs.append(new_url)
                              pass
                        if search == "" or search in item.contents[0]:
                              pdfs.extend(find_links(new_url, ""))
                  except:
                        pass
      return pdfs

#USE THIS FOR AIP GREENLAND
links = find_links("http://aim.naviair.dk", "AIP Gr")

pdfs = []
id = 0
fejl = 0
writer = PdfFileWriter()

print "Links found, starting download..."
count = 0
for link in links:
      try:
            url = quote(link.encode('utf-8'))
            url = url.replace('%3A', ':')
            reader = urllib2.urlopen(url).read()
            memory = StringIO(reader)
            pdfFile = PdfFileReader(memory)
            global count
            count += 1
            print count
            for page in xrange(pdfFile.getNumPages()):
                  writer.addPage(pdfFile.getPage(page))
      except:
        fejl = fejl + link
	pass
output = open("AIPGREENLAND.pdf", "wb")
writer.write(output)
output.close()
print ("The AIP is generated. ", fejl, " pages could not be downloaded")